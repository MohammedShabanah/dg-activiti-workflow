package org.activiti.Services;


import org.activiti.Models.Form;
import org.activiti.Repositories.FormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormService {

    @Autowired
    FormRepository formRepository;


    public List<Form> getFormByName(String name){
        return formRepository.findByName(name);
    }

    public List<Form> getFormByKey(String key){
        return formRepository.findByKey(key);
    }
}
