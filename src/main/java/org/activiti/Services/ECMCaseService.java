package org.activiti.Services;


import org.activiti.Models.ECM_CASE;
import org.activiti.Models.Form;
import org.activiti.Repositories.ECMCaseRepository;
import org.activiti.Repositories.FormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ECMCaseService {

    @Autowired
    ECMCaseRepository ecmCaseRepository;


    public List<ECM_CASE> getAllECMCases(){
        return ecmCaseRepository.findAll();
    }


    public ECM_CASE getCaseByCaseRK(String caseRK){
        ECM_CASE ecm_case =  ecmCaseRepository.findByCaseRK(caseRK);
        return ecm_case;
    }

    public ECM_CASE updateEcmCase(ECM_CASE ecm_case){
        return  ecmCaseRepository.save(ecm_case);
    }
}
