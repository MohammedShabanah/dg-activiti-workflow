package org.activiti.Services;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.activiti.Models.DTOs.FormOutcomes;
import org.activiti.Models.DTOs.TaskObject;
import org.activiti.Models.DTOs.WorkFlowCasesObject;
import org.activiti.Models.ECMCaseProcessInstance;
import org.activiti.Models.ECM_CASE;
import org.activiti.Models.Form;
import org.activiti.Models.WorkflowCaseHistory;
import org.activiti.engine.*;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.logging.Logger;

@Service
public class ActivitiWorkflowService {

    private static Logger log = Logger.getLogger(ActivitiWorkflowService.class.getName());

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private org.activiti.engine.FormService formService;

    @Autowired
    private FormService myFormService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ManagementService managementService;

    @Autowired
    private IdentityService identityService;


    @Autowired
    private ECMCaseService ecmCaseService;


    @Autowired
    private WorkflowCasesService workflowCasesService;

    @Autowired
    private ECMCaseProcessInstanceService ecmCaseProcessInstanceService;



    public ResponseEntity startProcessInstance(WorkflowCaseHistory workflowCase) {

        try {

            ECMCaseProcessInstance ecmCaseProcessInstance = new ECMCaseProcessInstance();
//            if(ecmCaseProcessInstance!=null){
//                if(ecmCaseProcessInstance.getInstanceId()!=null && !ecmCaseProcessInstance.getInstanceId().equals("null")){
//                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Case Already has an Instance");
//                }
//            }

            ProcessInstance processInstance = null;
            try {
                processInstance = runtimeService.startProcessInstanceByKey(workflowCase.getWorkflowKey());
            }catch (Exception e){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }

            //
            ecmCaseProcessInstance = new ECMCaseProcessInstance();
            ecmCaseProcessInstance.setCaseRK(workflowCase.getCaseRK());
            ecmCaseProcessInstance.setCaseCompleted(false);
            ecmCaseProcessInstance.setInstanceId(processInstance.getId());
            ecmCaseProcessInstance.setStartDate(new Date());
            ecmCaseProcessInstanceService.saveOrUpdate(ecmCaseProcessInstance);
            //

            List<Task> allTasks = taskService.createTaskQuery().taskCandidateGroup(workflowCase.getGroupName()).processInstanceId(processInstance.getId()).list();

            if (allTasks != null && allTasks.size() > 0) {
                Task currentTask = allTasks.get(0);

                // we are updating case state
                ECM_CASE ecm_case = ecmCaseService.getCaseByCaseRK(workflowCase.getCaseRK());
                if(ecm_case!=null){
                    ecm_case.setCaseState(currentTask.getName());
                    ecmCaseService.updateEcmCase(ecm_case);
                }

                List<WorkflowCaseHistory> workflow_case_histories = new ArrayList<>();

                TaskObject taskObject = new TaskObject();

                //  here we need to check if task assigned to group or individual user
                taskObject.setTaskId(currentTask.getId());

                taskObject.setAssignee(currentTask.getAssignee());

                taskObject.setFormKey(currentTask.getFormKey());
                taskObject.setInstanceId(processInstance.getId());
                taskObject.setStartDate(currentTask.getCreateTime());
                taskObject.setTaskName(currentTask.getName());

                if (currentTask.getFormKey() != null && !currentTask.getFormKey().equals("null")) {
                    taskObject.setFormOutcomes(getTaskOutcomes(currentTask.getFormKey()));
                    taskObject.setHasOutcomes(true);
                } else {
                    taskObject.setHasOutcomes(false);
                }

                WorkflowCaseHistory workflow_caseHistory = new WorkflowCaseHistory();

                workflow_caseHistory.setCaseRK(workflowCase.getCaseRK());
                workflow_caseHistory.setInstanceId(processInstance.getId());
                workflow_caseHistory.setWorkflowKey(workflowCase.getWorkflowKey());
                workflow_caseHistory.setTaskName(currentTask.getName());
                workflow_caseHistory.setGroupName(workflowCase.getGroupName());
                workflow_caseHistory.setTaskCompleted(false);
                workflow_caseHistory.setCaseCompleted(false);


//                workflowCasesService.saveOrUpdate(workflow_caseHistory);

                workflow_caseHistory.setTaskObject(taskObject);

                workflow_case_histories.add(workflow_caseHistory);

                return ResponseEntity.status(HttpStatus.OK).body(workflow_case_histories);

            } else {
                // this group started process Instance and has no tasks assigned now

                allTasks = taskService.createTaskQuery().processInstanceId(processInstance.getId()).list();
                if(allTasks!=null && allTasks.size()>0){
                    Task currentTask = allTasks.get(0);

                    // we are updating case state
                    ECM_CASE ecm_case = ecmCaseService.getCaseByCaseRK(workflowCase.getCaseRK());
                    if(ecm_case!=null){
                        ecm_case.setCaseState(currentTask.getName());
                        ecmCaseService.updateEcmCase(ecm_case);
                    }
                    List<WorkflowCaseHistory> workflow_case_histories = new ArrayList<>();

                    WorkflowCaseHistory workflow_caseHistory = new WorkflowCaseHistory();

                    workflow_caseHistory.setCaseRK(workflowCase.getCaseRK());
                    workflow_caseHistory.setInstanceId(processInstance.getId());
                    workflow_caseHistory.setWorkflowKey(workflowCase.getWorkflowKey());
                    workflow_caseHistory.setTaskCompleted(false);
                    workflow_caseHistory.setCaseCompleted(false);

                    workflow_case_histories.add(workflow_caseHistory);

                    return ResponseEntity.status(HttpStatus.OK).body(workflow_case_histories);

                } else {
                    // this case has no tasks
                    runtimeService.deleteProcessInstance(processInstance.getId(),"");
                    ecmCaseProcessInstance.setInstanceId(null);
                    ecmCaseProcessInstanceService.saveOrUpdate(ecmCaseProcessInstance);
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Case Instance Cannot be started :: no tasks found in this process");
                }

            }

        }catch (Exception e){
            log.warning("*********** "+e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    // get current running group tasks

    public ResponseEntity getCaseWorkflowTasks(WorkflowCaseHistory workflowCase) {

        try {

            // check first if his case has no instance
            //
            ECMCaseProcessInstance ecmCaseProcessInstance = ecmCaseProcessInstanceService.getCaseByCaseRK(workflowCase.getCaseRK());
            if (ecmCaseProcessInstance==null ||ecmCaseProcessInstance.getInstanceId() == null || ecmCaseProcessInstance.getInstanceId().equals("null")) {
                // it means this caseRK needs a new process instance
                return startProcessInstance(workflowCase);
            }

            // get if this case has a workflow history
            List<WorkflowCaseHistory> workflowCaseHistories = workflowCasesService.getAllWorkflowCasesByCaseRK(workflowCase.getCaseRK());

            String instanceId = ecmCaseProcessInstance.getInstanceId();
            if(workflowCaseHistories == null || workflowCaseHistories.size()<1){
                workflowCaseHistories = new ArrayList<>();
            }
            if(instanceId == null || instanceId.equals("null")){
                // invalid each case should have an instance assigned
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("cannot find instance id");
            }

            List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup(workflowCase.getGroupName()).processInstanceId(instanceId).list();;

            if (tasks != null && tasks.size() > 0) {

                Task task = tasks.get(0);

                TaskObject taskObject = new TaskObject();

                taskObject.setTaskId(task.getId());
                taskObject.setAssignee(task.getAssignee());
                taskObject.setFormKey(task.getFormKey());
                taskObject.setInstanceId(instanceId);
                taskObject.setStartDate(task.getCreateTime());
                taskObject.setTaskName(task.getName());

                if (task.getFormKey() != null && !task.getFormKey().equals("null")) {
                    taskObject.setFormOutcomes(getTaskOutcomes(task.getFormKey()));
                    taskObject.setHasOutcomes(true);
                } else {
                    taskObject.setHasOutcomes(false);
                }

                WorkflowCaseHistory workflowCases = new WorkflowCaseHistory();

                workflowCases.setCaseRK(workflowCase.getCaseRK());
                workflowCases.setInstanceId(instanceId);
                workflowCases.setTaskObject(taskObject);
                workflowCases.setTaskName(taskObject.getTaskName());
                workflowCases.setTaskCompleted(false);
                workflowCases.setCaseCompleted(false);
                workflowCases.setTaskName(taskObject.getTaskName());

                workflowCases.setGroupName(workflowCase.getGroupName());

                workflowCaseHistories.add(workflowCases);
            } else {
                // if instance has another tasks
                tasks = taskService.createTaskQuery().processInstanceId(instanceId).list();

                if(tasks!=null && tasks.size()>0){

                    Task task = tasks.get(0);

                    // can ignore taskObject
                    TaskObject taskObject = new TaskObject();

                    taskObject.setTaskId(task.getId());
                    taskObject.setAssignee(task.getAssignee());
                    taskObject.setFormKey(task.getFormKey());
                    taskObject.setInstanceId(instanceId);
                    taskObject.setStartDate(task.getCreateTime());
                    taskObject.setTaskName(task.getName());

                    taskObject.setHasOutcomes(false);

                    WorkflowCaseHistory workflowCases = new WorkflowCaseHistory();

                    workflowCases.setCaseRK(workflowCase.getCaseRK());
                    workflowCases.setInstanceId(instanceId);
                    workflowCases.setTaskObject(null);
                    workflowCases.setTaskName(taskObject.getTaskName());
                    workflowCases.setTaskCompleted(false);
                    workflowCases.setCaseCompleted(false);
                    workflowCases.setTaskName(taskObject.getTaskName());

                    workflowCaseHistories.add(workflowCases);
                } else {
                    return ResponseEntity.ok(workflowCaseHistories);
                }
            }

            Collections.reverse(workflowCaseHistories);
            return ResponseEntity.status(HttpStatus.OK).body(workflowCaseHistories);

        }catch (Exception e){
            log.warning("************* "+e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    public ResponseEntity submitWorkflowTask(WorkflowCaseHistory workflowCases) {
        try {

            // we need to know instance Id, taskId ,
            ECMCaseProcessInstance ecmCaseProcessInstance = ecmCaseProcessInstanceService.getCaseByCaseRK(workflowCases.getCaseRK());
            if(ecmCaseProcessInstance == null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This Case has no Instance Assigned");
            }

            String instanceId  = ecmCaseProcessInstance.getInstanceId();
            workflowCases.setInstanceId(instanceId);

            // here we need to find group Task
            List<Task> groupTasks =  taskService.createTaskQuery().taskCandidateGroup(workflowCases.getGroupName()).active().list();
            if(groupTasks == null || groupTasks.size()<1){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("group : "+workflowCases.getGroupName()+ " has no tasks Assigned Now");
            }

            Task currentTask = null;
            for (Task task:groupTasks){
                if(task.getProcessInstanceId().equals(instanceId)){
                    currentTask = task;
                    break;
                }
            }

            if(currentTask == null ){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("group : "+workflowCases.getGroupName()+ "" +
                        " has no tasks Assigned Now");
            }

            WorkflowCaseHistory workflowCasesTobeSaved = new WorkflowCaseHistory();

            workflowCasesTobeSaved.setCurrentState(workflowCases.getTaskOutcome());
            workflowCasesTobeSaved.setTaskName(currentTask.getName());
            workflowCasesTobeSaved.setCaseRK(workflowCases.getCaseRK());
            workflowCasesTobeSaved.setInstanceId(instanceId);
            workflowCasesTobeSaved.setTaskOutcome(workflowCases.getTaskOutcome());

            workflowCasesTobeSaved.setGroupName(workflowCases.getGroupName());

            workflowCasesTobeSaved.setTaskCompleted(true);
            workflowCasesTobeSaved.setCompletedOn(new Date());
            workflowCasesTobeSaved.setCompletedBy(workflowCases.getCompletedBy());

            if (workflowCases.getTaskOutcome() == null || workflowCases.getTaskOutcome().equals("null")) {
                taskService.complete(currentTask.getId());
            } else {
                // assumed that we have only one form per task and one outcome chosen in task
                Map<String, Object> variables = new HashMap<>();

                variables.put("form_" + currentTask.getFormKey() + "_outcome", workflowCases.getTaskOutcome());

                taskService.setVariable(currentTask.getId(),"taskOutcome",workflowCases.getTaskOutcome());
                taskService.complete(currentTask.getId());
            }

            // check if instance has an active tasks or not

            List<Task> activeTasks = taskService.createTaskQuery().processInstanceId(workflowCases.getInstanceId()).active().list();

            if (activeTasks == null || activeTasks.size() < 1) {
                workflowCasesTobeSaved.setCaseCompleted(true);
                workflowCasesTobeSaved.setCompletedOn(new Date());
                workflowCasesService.saveOrUpdate(workflowCasesTobeSaved);

                //
                ecmCaseProcessInstance.setCaseCompleted(true);
                ecmCaseProcessInstance.setCompletedDate(new Date());
                ecmCaseProcessInstanceService.saveOrUpdate(ecmCaseProcessInstance);
                //

                // updating case state
                ECM_CASE ecm_case = ecmCaseService.getCaseByCaseRK(workflowCases.getCaseRK());
                if(ecm_case!=null){
                    ecm_case.setCaseState("Finished");
                    ecmCaseService.updateEcmCase(ecm_case);
                }


            } else {
                workflowCasesService.saveOrUpdate(workflowCasesTobeSaved);

                Task task = activeTasks.get(0);

                ECM_CASE ecm_case = ecmCaseService.getCaseByCaseRK(workflowCases.getCaseRK());
                if(ecm_case!=null){
                    ecm_case.setCaseState(task.getName());
                    ecmCaseService.updateEcmCase(ecm_case);
                }
            }

            return  ResponseEntity.status(HttpStatus.OK).body(workflowCasesTobeSaved);

        }catch (Exception e){
            log.warning(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }




    public List<FormOutcomes> getTaskOutcomes(String formKey) {
        List<Form> forms = myFormService.getFormByKey(formKey);
        if(forms!=null && forms.size()>0){
            Form form = forms.get(0);
            JSONObject jsonObject = new JSONObject(form.getModelEditorJson());
            JSONArray jsonArray = jsonObject.getJSONArray("outcomes");

            Gson gson = new Gson();
            Type listType = new TypeToken<List<FormOutcomes>>() {
            }.getType();
            List<FormOutcomes> formOutcomes = gson.fromJson(jsonArray.toString(), listType);
            return formOutcomes;
        } else {
            return null;
        }
    }


}
