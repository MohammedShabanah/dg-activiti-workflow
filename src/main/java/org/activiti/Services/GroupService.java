package org.activiti.Services;


import org.activiti.Models.DTOs.ActivitiGroup;
import org.activiti.Models.Group;
import org.activiti.Repositories.GroupRepository;
import org.activiti.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService {

    @Autowired
    GroupRepository groupRepository;

    public List<Group> getAllGroups(){
        return groupRepository.findAll();
    }

//    public List<ActivitiGroup> mapToActivitiGroups(List<Group> ecmGroups){
//
//        List<ActivitiGroup> activitiGroups = new ArrayList<>();
//        for(Group group:ecmGroups){
//            activitiGroups.add(new ActivitiGroup(
//                    group.getGroupName(),
//                    group.getGroupName(),
//                    group.getGroupType(),
//                    null
//            ));
//        }
//        return activitiGroups;
//    }

}
