package org.activiti.Services;


import org.activiti.Models.DTOs.ActivitiUser;
import org.activiti.Models.ECM_CASE;
import org.activiti.Models.User;
import org.activiti.Repositories.ECMCaseRepository;
import org.activiti.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAllEcmUsers(){
        return userRepository.findAll();
    }

//    public List<ActivitiUser> mapToActivitiUsers(List<User> ecmUsers){
//        List<ActivitiUser> users = new ArrayList<>();
//        for(User user:ecmUsers){
//            users.add(new ActivitiUser(
//                    user.getFirstName(),
//                    user.getFirstName(),
//                    user.getLastName(),
//                    user.getFirstName()+' '+user.getLastName(),
//                    user.getEmail(),
//                    new ArrayList<>()
//            ));
//        }
//        return users;
//    }


}
