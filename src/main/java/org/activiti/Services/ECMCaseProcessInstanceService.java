package org.activiti.Services;


import org.activiti.Models.ECMCaseProcessInstance;
import org.activiti.Models.WorkflowCaseHistory;
import org.activiti.Repositories.ECMCaseProcessInstanceRepository;
import org.activiti.Repositories.WorkflowCasesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ECMCaseProcessInstanceService {

    @Autowired
    ECMCaseProcessInstanceRepository ecmCaseProcessInstanceRepository;

    public ECMCaseProcessInstance saveOrUpdate(ECMCaseProcessInstance ecmCaseProcessInstance){
        return ecmCaseProcessInstanceRepository.save(ecmCaseProcessInstance);
    }

    public ECMCaseProcessInstance getOne(Long id){
        return ecmCaseProcessInstanceRepository.findById(id);
    }

    public ECMCaseProcessInstance getCaseByCaseRK(String caseRK){
        return ecmCaseProcessInstanceRepository.findFirstByCaseRK(caseRK);
    }
}
