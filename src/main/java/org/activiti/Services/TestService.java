package org.activiti.Services;


import org.activiti.Models.ObjectMapper;
import org.activiti.Models.WorkflowCaseHistory;
import org.activiti.Repositories.FormRepository;
import org.activiti.Repositories.TestRepository;
import org.hibernate.tool.hbm2ddl.TableMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestService {

    @Autowired
    private TestRepository testRepository;

    public ResponseEntity getTables() {

        List<String> Columns = new ArrayList<String>();
        Field[] fields = WorkflowCaseHistory.class.getDeclaredFields();
        for (Field field : fields) {
            Column col = field.getAnnotation(Column.class);
            if (col != null) {
                Columns.add(col.name());
                System.out.println("Columns: "+col);
            }
        }
        System.out.println(Columns);

        //
        List<ObjectMapper> data = testRepository.getAllTables();
        //

        return ResponseEntity.ok(Columns);
    }


}
