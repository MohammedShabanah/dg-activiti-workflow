package org.activiti.Services;


import org.activiti.Models.WorkflowCaseHistory;
import org.activiti.Repositories.WorkflowCasesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkflowCasesService {

    @Autowired
    WorkflowCasesRepository workflowCasesRepository;

    public List<WorkflowCaseHistory> getAllWorkflowCases(){
        return workflowCasesRepository.findAll();
    }

    public List<WorkflowCaseHistory> getAllWorkflowCasesByCaseRK(String caseRK){
        return workflowCasesRepository.findByCaseRK(caseRK);
    }

    public List<WorkflowCaseHistory> getAllWorkflowCasesByInstanceId(String instanceId){
        return workflowCasesRepository.findByInstanceId(instanceId);
    }

    public WorkflowCaseHistory saveOrUpdate(WorkflowCaseHistory workflow_caseHistory){
        return workflowCasesRepository.save(workflow_caseHistory);
    }

    public WorkflowCaseHistory getOne(Long id){
        return workflowCasesRepository.findById(id);
    }

    public WorkflowCaseHistory getFirstCaseHistory(String caseRK){
        return workflowCasesRepository.findFirstByCaseRK(caseRK);
    }
}
