package org.activiti;


import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication(
        exclude = org.activiti.spring.boot.SecurityAutoConfiguration.class
)
public class MyApp {

    public static void main(String[] args) {
        SpringApplication.run(MyApp.class, args);
    }


    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/*").allowedOrigins("*");
            }
        };
    }

//    @Bean
//    InitializingBean usersAndGroupsInitializer(IdentityService identityService) {
//        return new InitializingBean() {
//            public void afterPropertiesSet() throws Exception {
//                User user = identityService.newUser("adminUser");
//                user.setPassword("admin");
//                identityService.saveUser(user);
//
//                Group group = identityService.newGroup("firstGroup");
//                group.setName("ROLE_USER");
//                group.setType("USER");
//                identityService.saveGroup(group);
//                identityService.createMembership(user.getId(), group.getId());
//            }
//        };
//    }
}
