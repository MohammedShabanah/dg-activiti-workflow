package org.activiti.Models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "ACT_DE_MODEL")
public class Form {
    @Id
    @Column(name="id")
    private String id ;

    @Column(name = "name", nullable = false)
    private String name ;

    @Column(name = "model_key", nullable = false)
    private String key ;

    @Column(name = "description", nullable = false)
    private String description ;

    @Column(name = "model_comment", nullable = true)
    private String comment;
    @Column(name = "created", nullable = true)
    private Date created ;
    @Column(name = "created_by", nullable = true)
    private String createdBy ;
    @Column(name = "last_updated", nullable = true)
    private String lastUpdated;
    @Column(name = "last_updated_by", nullable = true)
    private String lastUpdatedBy;
    @Column(name = "version", nullable = true)
    private int version;
    @Column(name = "model_editor_json", nullable = true)
    private String modelEditorJson;
    @Column(name = "thumbnail", nullable = false)
    private String thumbnail;
    @Column(name = "model_type", nullable = false)
    private int modelType;

    public Form() {
    }

    public Form(String id, String name, String key, String description, String comment, Date created, String createdBy, String lastUpdated, String lastUpdatedBy, int version, String modelEditorJson, String thumbnail, int modelType) {
        this.id = id;
        this.name = name;
        this.key = key;
        this.description = description;
        this.comment = comment;
        this.created = created;
        this.createdBy = createdBy;
        this.lastUpdated = lastUpdated;
        this.lastUpdatedBy = lastUpdatedBy;
        this.version = version;
        this.modelEditorJson = modelEditorJson;
        this.thumbnail = thumbnail;
        this.modelType = modelType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getModelType() {
        return modelType;
    }

    public void setModelType(int modelType) {
        this.modelType = modelType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getModelEditorJson() {
        return modelEditorJson;
    }

    public void setModelEditorJson(String modelEditorJson) {
        this.modelEditorJson = modelEditorJson;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Form{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", description='" + description + '\'' +
                ", comment='" + comment + '\'' +
                ", created=" + created +
                ", createdBy='" + createdBy + '\'' +
                ", lastUpdated='" + lastUpdated + '\'' +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", version=" + version +
                ", modelEditorJson='" + modelEditorJson + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", modelType=" + modelType +
                '}';
    }
}
