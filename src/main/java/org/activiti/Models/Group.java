package org.activiti.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="TESTGroup")
public class Group implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "Name")
    private String groupName;

    @Column(name = "Description")
    private String description;


    @Column(name = "Is_Active")
    private Boolean isActive;


    @Column(name = "Mapped_To_Activiti")
    private Boolean mappedToActiviti;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getMappedToActiviti() {
        return mappedToActiviti;
    }

    public void setMappedToActiviti(Boolean mappedToActiviti) {
        this.mappedToActiviti = mappedToActiviti;
    }
}