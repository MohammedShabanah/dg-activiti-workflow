package org.activiti.Models.DTOs;


import java.util.List;

public class ActivitiUser {

    private String id;

    private String firstName;

    private String lastName;

    private String fullName;

    private String email;

    private List<String> groups;

    public ActivitiUser(String id, String firstName, String lastName, String fullName, String email, List<String> groups) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.email = email;
        this.groups = groups;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        if(firstName!=null && !firstName.equals("null")){
            return firstName;
        }
        return "";
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        if(lastName!=null && !lastName.equals("null")){
            return lastName;
        }
        return "";
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }
}
