package org.activiti.Models.DTOs;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TaskObject implements Serializable {

    private String taskId;

    private String instanceId;

    private String taskName;

    private Date startDate;

    private String formKey;

    private List<FormOutcomes> formOutcomes;

    private String assignee;

    private String groupName;

    private Boolean hasOutcomes;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public List<FormOutcomes> getFormOutcomes() {
        return formOutcomes;
    }

    public void setFormOutcomes(List<FormOutcomes> formOutcomes) {
        this.formOutcomes = formOutcomes;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getHasOutcomes() {
        return hasOutcomes;
    }

    public void setHasOutcomes(Boolean hasOutcomes) {
        this.hasOutcomes = hasOutcomes;
    }
}
