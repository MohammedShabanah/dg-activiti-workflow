package org.activiti.Models.DTOs;

import org.activiti.Models.WorkflowCaseHistory;

public class WorkFlowCasesObject {

    private WorkflowCaseHistory workflowCases;

    private TaskObject taskObject;


    public WorkflowCaseHistory getWorkflowCases() {
        return workflowCases;
    }

    public void setWorkflowCases(WorkflowCaseHistory workflowCases) {
        this.workflowCases = workflowCases;
    }

    public TaskObject getTaskObject() {
        return taskObject;
    }

    public void setTaskObject(TaskObject taskObject) {
        this.taskObject = taskObject;
    }
}
