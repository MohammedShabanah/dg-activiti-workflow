package org.activiti.Models;

import org.activiti.Models.DTOs.TaskObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="ECMCaseProcessInstance")
public class ECMCaseProcessInstance implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String caseRK;

    private String instanceId;

    private Boolean caseCompleted;

    private Date startDate;

    private Date completedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseRK() {
        return caseRK;
    }

    public void setCaseRK(String caseRK) {
        this.caseRK = caseRK;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public Boolean getCaseCompleted() {
        return caseCompleted;
    }

    public void setCaseCompleted(Boolean caseCompleted) {
        this.caseCompleted = caseCompleted;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Date completedDate) {
        this.completedDate = completedDate;
    }
}