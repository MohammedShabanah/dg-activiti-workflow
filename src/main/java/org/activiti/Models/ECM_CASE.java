package org.activiti.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="ECM_CASE")
public class ECM_CASE implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "CASE_RK")
    private String caseRK;


    @Column(name = "CASE_NAME")
    private String caseName;

    @Column(name = "CASE_STATE")
    private String caseState;

    @Column(name = "WORKFLOW_ID")
    private String workflowId;

    @Column(name = "WORKFLOW_KEY")
    private String workflowKey;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseRK() {
        return caseRK;
    }

    public void setCaseRK(String caseRK) {
        this.caseRK = caseRK;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getCaseState() {
        return caseState;
    }

    public void setCaseState(String caseState) {
        this.caseState = caseState;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getWorkflowKey() {
        return workflowKey;
    }

    public void setWorkflowKey(String workflowKey) {
        this.workflowKey = workflowKey;
    }
}