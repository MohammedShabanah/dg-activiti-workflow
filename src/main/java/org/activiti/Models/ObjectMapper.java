package org.activiti.Models;

public class ObjectMapper {


    private String tableName;
    private String columnName;

    public ObjectMapper(String tableName, String columnName) {
        super();
        this.tableName = tableName;
        this.columnName = columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
