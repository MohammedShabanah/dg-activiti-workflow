package org.activiti.Models;

import org.activiti.Models.DTOs.TaskObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="WorkflowCaseHistory")
public class WorkflowCaseHistory implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "CASE_RK")
    private String caseRK;

    @Column(name = "WORKFLOW_KEY")
    private String workflowKey;

    @Column(name = "INSTANCE_ID")
    private String instanceId;

    @Column(name = "COMPLETED_BY")
    private String completedBy;

    @Column(name = "COMPLETED_ON")
    private Date completedOn;

    @Column(name = "CURRENT_STATE")
    private String currentState;

    @Column(name = "TASK_NAME")
    private String taskName;

    @Column(name = "TASK_OUTCOME")
    private String taskOutcome;

    @Column(name = "CASE_COMPLETED")
    private Boolean caseCompleted;

    @Column(name = "TASK_COMPLETED")
    private Boolean taskCompleted;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @Column(name = "DATE")
    private Date date;

    private TaskObject taskObject;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseRK() {
        return caseRK;
    }

    public void setCaseRK(String caseRK) {
        this.caseRK = caseRK;
    }

    public String getWorkflowKey() {
        return workflowKey;
    }

    public void setWorkflowKey(String workflowKey) {
        this.workflowKey = workflowKey;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public Date getCompletedOn() {
        return completedOn;
    }

    public void setCompletedOn(Date completedOn) {
        this.completedOn = completedOn;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskOutcome() {
        return taskOutcome;
    }

    public void setTaskOutcome(String taskOutcome) {
        this.taskOutcome = taskOutcome;
    }

    public Boolean getCaseCompleted() {
        return caseCompleted;
    }

    public void setCaseCompleted(Boolean caseCompleted) {
        this.caseCompleted = caseCompleted;
    }

    public Boolean getTaskCompleted() {
        return taskCompleted;
    }

    public void setTaskCompleted(Boolean taskCompleted) {
        this.taskCompleted = taskCompleted;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TaskObject getTaskObject() {
        return taskObject;
    }

    public void setTaskObject(TaskObject taskObject) {
        this.taskObject = taskObject;
    }
}