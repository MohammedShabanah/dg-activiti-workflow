package org.activiti.Controllers;

import org.activiti.Models.ECM_CASE;
import org.activiti.Models.WorkflowCaseHistory;
import org.activiti.Services.*;
import org.activiti.engine.*;
import org.activiti.engine.FormService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Table;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
public class WorkflowCaseController {

    private static Logger log = Logger.getLogger(WorkflowCaseController.class.getName());

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private FormService formService;

    @Autowired
    private org.activiti.Services.FormService myFormService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ManagementService managementService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private ECMCaseService ecmCaseService;

    @Autowired
    private WorkflowCasesService workflowCasesService;

    @Autowired
    private ActivitiWorkflowService activitiWorkflowService;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private TestService testService;


    @RequestMapping(value = "/getWorkflowTasks")
    public ResponseEntity getInstanceActiveTasks(@RequestBody WorkflowCaseHistory workflowCaseHistory) {
        try {
            // parameters is a request body contains :: caseRK,groupName,workflowKey
            String groupName = workflowCaseHistory.getGroupName();
            String caseRK = workflowCaseHistory.getCaseRK();
            String workflow = workflowCaseHistory.getWorkflowKey();
            Boolean validate = (groupName != null && !groupName.equals("null")) && (caseRK != null && !caseRK.equals("null")) && (workflow != null && !workflow.equals("null"));
            if (!validate) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("invalid parameters");
            }
            ResponseEntity response = activitiWorkflowService.getCaseWorkflowTasks(workflowCaseHistory);
            return response;
        } catch (Exception e) {
            log.warning("********* ERROR *********" + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @RequestMapping(value = "/submitWorkflowTask")
    public ResponseEntity submitTaskWithOutcome(@RequestBody WorkflowCaseHistory workflowCase) {
        try {
            log.info("*********** submitWorkflowTask ::" +
                    "workflowCases ::" + workflowCase);

            // here body includes : caseRK , taskOutcome, completedBy , groupName ,
            String groupName = workflowCase.getGroupName();
            String caseRK = workflowCase.getCaseRK();
            String taskOutcome = workflowCase.getTaskOutcome();
            String completedBy = workflowCase.getCompletedBy();

            Boolean validate = (groupName != null && !groupName.equals("null"))
                    && (caseRK != null && !caseRK.equals("null"))
                    && (completedBy != null && !completedBy.equals("null"));
//                    && (taskOutcome != null && !taskOutcome.equals("null"));
            if (!validate) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("invalid parameters");
            }

            ResponseEntity response = activitiWorkflowService.submitWorkflowTask(workflowCase);
            return response;
        } catch (Exception e) {
            log.warning("********* ERROR *********" + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


    @GetMapping(value = "/getFinishedTasks")
    public ResponseEntity<?> getFinishedTasks(@RequestParam(required = true) String instanceId) {
        try {
            List<Task> tasks = taskService.createTaskQuery().processInstanceId(instanceId).list();
            List<HistoricTaskInstance> tasks2 = historyService.createHistoricTaskInstanceQuery().processInstanceId(instanceId).finished().list();
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (Exception e) {
            log.warning("********* ERROR *********" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/countInstances")
    public ResponseEntity countRunningInstances() {
        return ResponseEntity.ok(runtimeService.createProcessInstanceQuery().count());
    }

    @GetMapping("/getProcessDefinitions")
    public ResponseEntity getProcessDefinitions() {
        List<ProcessDefinition> processes = repositoryService.createProcessDefinitionQuery().active().list();
        List<String> names = processes.stream().map(x -> x.getName()).collect(Collectors.toList());

        Map<String, String> response = processes.stream().collect(Collectors.toMap(ProcessDefinition::getId, ProcessDefinition::getKey));
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getProcessInstances")
    public ResponseEntity getProcessInstances() {
        List<ProcessInstance> instances = runtimeService.createProcessInstanceQuery().list();
        List<String> ids = instances.stream().map(x -> x.getId()).collect(Collectors.toList());
        Map<String, String> response = instances.stream().collect(Collectors.toMap(ProcessInstance::getId, ProcessInstance::getProcessDefinitionKey));
        return ResponseEntity.ok(response);
    }


    @RequestMapping(value = "/deleteAllRunningInstances")
    public ResponseEntity<?> deleteAllRunningInstances() {
        try {
            log.info("***********  deleteAllRunningInstances");
            List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery().active().list();

//            for (ProcessInstance processInstance : processInstances) {
//                runtimeService.deleteProcessInstance(processInstance.getId(), "");
//            }
            processInstances.stream().forEach(
                    instance->runtimeService.deleteProcessInstance(instance.getId(),"")
            );
            return new ResponseEntity<>(processInstances.size(), HttpStatus.OK);
        } catch (Exception e) {
            log.warning("********* ERROR *********" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/getECMCases")
    public ResponseEntity getECMCases() {
        try {
            log.info("*****************  getECMCases");
            List<ECM_CASE> ecm_cases = ecmCaseService.getAllECMCases();
            return new ResponseEntity(ecm_cases, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(e.getCause(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/testService")
    public ResponseEntity testService() {
        try {
            log.info("*****************  testService");

            //
            List<Task> testTasks = taskService.createTaskQuery().taskCandidateUser("test1").list();
            List<User> users = identityService.createUserQuery().memberOfGroup("testGroup").list();
            List<Group> groups = identityService.createGroupQuery().list();

            //
            testTasks = taskService.createTaskQuery().list();

            // we can generate sql query to select from activiti tables
            taskService.createNativeTaskQuery().sql("select   count(*) from " + managementService.getTableName(Table.class) + " " +
                    " T where T.NAME_ = #{taskName}").parameter("taskName", "test Task").list();


            //
            runtimeService.createProcessInstanceBuilder().processDefinitionKey("TestProcess")
                    .transientVariable("x", "x")
                    .transientVariable("y", "").start();


            log.info(testService.getTables().toString());
            return testService.getTables();
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
