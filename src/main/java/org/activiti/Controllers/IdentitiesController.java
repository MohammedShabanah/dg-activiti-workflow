package org.activiti.Controllers;

import org.activiti.Services.ECMCaseService;
import org.activiti.Services.WorkflowCasesService;
import org.activiti.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
public class IdentitiesController {


    private static Logger log = Logger.getLogger(GlobalRestController.class.getName());


    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private FormService formService;

    @Autowired
    private org.activiti.Services.FormService myFormService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ManagementService managementService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private ECMCaseService ecmCaseService;

    @Autowired
    private WorkflowCasesService workflowCasesService;


    public void createNewUser(){

    }

    public void getActivitiUsers(){

    }

    public void createNewGroup(){

    }

    public void assignUserToGroup(){

    }

    public void addGroupUsers(){

    }



}
