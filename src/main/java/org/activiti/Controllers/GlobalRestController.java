package org.activiti.Controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.activiti.Models.Form;
import org.activiti.Models.DTOs.FormOutcomes;
import org.activiti.Services.ECMCaseService;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class GlobalRestController {

    private static Logger log = Logger.getLogger(GlobalRestController.class.getName());


    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private FormService formService;

    @Autowired
    private org.activiti.Services.FormService myFormService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ManagementService managementService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private ECMCaseService ecmCaseService;


    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/start-process", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> startInstance(@RequestParam String processKey) {

        try {
            if (processKey == null || processKey.equals("null")) {
                return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
            }
            System.out.println("receiving process process key = " + processKey);
            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processKey);
            return new ResponseEntity<Object>(processInstance, HttpStatus.OK);
        } catch (Exception e) {
            System.err.println("exception happened while starting new instance with  process key =" + processKey);
            return new ResponseEntity<Object>(e.getCause(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/get-processDefinition", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProcessDefinition(@RequestParam(required = false) String id, @RequestParam(required = false) String key) {
        try {

            if (id!=null && !id.equals("null")) {
                ProcessDefinition processDefinition = repositoryService.getProcessDefinition(id);
                return new ResponseEntity<Object>(processDefinition,HttpStatus.OK);
            } else if(key!=null && !key.equals("null")){
                List<ProcessDefinition> processDefinitionList = processEngine.getRepositoryService().createProcessDefinitionQuery().processDefinitionKeyLike(key).latestVersion().list();
                return new ResponseEntity<Object>(processDefinitionList.get(0), HttpStatus.OK);
            }

            // to get all current active processes
            List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().latestVersion().active().list();

            return new ResponseEntity<>(processDefinitions,HttpStatus.OK);
        } catch (Exception e) {
            log.warning("********* ERROR Getting ProcessDefinition " + e.getMessage());
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getHistory")
    public ResponseEntity<?> getHistory() {
        try {

            // getting finished tasks from history
            List<HistoricTaskInstance> finishedTasks = historyService.createHistoricTaskInstanceQuery().finished().list();
            List<HistoricTaskInstance> unfinishedTasks = historyService.createHistoricTaskInstanceQuery().unfinished().list();

            List<Object> list  = new ArrayList<>();
            list.addAll(finishedTasks);
            list.addAll(unfinishedTasks);

            return new ResponseEntity<>(list,HttpStatus.OK);
        } catch (Exception e) {
            log.warning("********   ERROR *********");
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/get-tasks-by-assignee")
    public ResponseEntity<Object> getTasksByAssignee(@RequestParam(required = false) String userName) {
        try {

            if ((userName.equals("null") || userName == null)) {
                return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
            }
            List<Task> tasks = taskService.createTaskQuery().taskAssignee(userName).list();

            return new ResponseEntity<Object>(tasks.toArray(), HttpStatus.OK);
        } catch (Exception e) {
            log.warning("********* ERROR Getting ProcessDefinition " + e.getMessage());
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/getFormByName")
    public ResponseEntity<Object> getFormByKey(@RequestParam(name = "formName") String formName) {
        try {
            Form form = myFormService.getFormByName(formName).get(0);
            return new ResponseEntity<Object>(form, HttpStatus.OK);
        } catch (Exception e) {
            log.warning("Error getFormByKey");
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/getFormFields")
    public ResponseEntity<Object> getFormFields(@RequestParam(name = "formName") String formName) {
        try {
            Form form = myFormService.getFormByName(formName).get(0);
            JSONObject jsonObject = new JSONObject(form.getModelEditorJson());
            JSONArray jsonArray = jsonObject.getJSONArray("fields");

            Gson gson = new Gson();
            Type listType = new TypeToken<List<Object>>() {
            }.getType();
            List<Object> formFileds = gson.fromJson(jsonArray.toString(), listType);

            return new ResponseEntity<Object>(formFileds, HttpStatus.OK);
        } catch (Exception e) {
            log.warning("Error getFormOutComes");
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/getFormOutcomes")
    public ResponseEntity<Object> getFormOutComes(@RequestParam(name = "formName") String formName) {
        try {
            Form form = myFormService.getFormByName(formName).get(0);
            JSONObject jsonObject = new JSONObject(form.getModelEditorJson());
            JSONArray jsonArray = jsonObject.getJSONArray("outcomes");

            Gson gson = new Gson();
            Type listType = new TypeToken<List<FormOutcomes>>() {
            }.getType();
            List<FormOutcomes> formOutcomes = gson.fromJson(jsonArray.toString(), listType);

            return new ResponseEntity<Object>(formOutcomes, HttpStatus.OK);
        } catch (Exception e) {
            log.warning("Error getFormOutComes");
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}