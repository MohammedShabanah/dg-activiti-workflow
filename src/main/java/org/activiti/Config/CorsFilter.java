package org.activiti.Config;//package org.activiti.Config;
//
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.filter.OncePerRequestFilter;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@Configuration
//@EnableWebMvc
//@EnableAutoConfiguration(exclude = {
//        org.activiti.spring.boot.RestApiAutoConfiguration.class,
//        org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class,
//        org.activiti.spring.boot.SecurityAutoConfiguration.class,
//        org.springframework.boot.actuate.autoconfigure.ManagementWebSecurityAutoConfiguration.class
//})
//public class CorsFilter extends OncePerRequestFilter {
//
//    @Override
//    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
//            throws ServletException, IOException {
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
//            response.setHeader("Access-Control-Allow-Credentials", "true");
//            response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
//            response.setHeader("Access-Control-Allow-Headers", "accept, x-requested-with, Content-Type, Accept-Language, Accept-Encoding ,Origin, Access-Control-Request-Method ,Access-Control-Request-Headers, Last-Modified, Cookie, Referer");
//            response.setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin,Accept-Ranges,Content-Encoding,Content-Length,Content-Range");
//            response.setHeader("Access-Control-Max-Age", "100");
//        }
//        filterChain.doFilter(request, response);
//    }
//
//
//}
//
