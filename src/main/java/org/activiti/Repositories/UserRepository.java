package org.activiti.Repositories;


import org.activiti.Models.ECM_CASE;
import org.activiti.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

}