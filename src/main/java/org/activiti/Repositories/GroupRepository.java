package org.activiti.Repositories;


import org.activiti.Models.Group;
import org.activiti.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long> {

}