package org.activiti.Repositories;

import org.activiti.Models.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;


@Repository
public class TestRepository {

    @Autowired
    private EntityManager entityManager;

//    @Query(value = "SELECT      c.name  AS 'columnName'\n" +
//            "     ,t.name AS 'tableName' \n" +
//            "FROM        sys.columns c \n" +
//            "              JOIN        sys.tables  t   ON c.object_id = t.object_id\n" +
//            "-- WHERE       c.name LIKE '%MyName%'\n" +
//            "ORDER BY    TableName\n" +
//            "       ,ColumnName;" +
//            "" +
//            "",
//    nativeQuery = true)

    String sql =  "SELECT CAST(t.name as varchar) as tableName , CAST(c.name as varchar) as columnName FROM sys.columns c  JOIN  sys.tables  t  ON c.object_id = t.object_id;";

    @SuppressWarnings("unchecked")
    public List<ObjectMapper> getAllTables(){
        try {
            Query query = entityManager.createNativeQuery(sql);
            List<ObjectMapper> result  = query.getResultList();
            return result;
        }catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

}