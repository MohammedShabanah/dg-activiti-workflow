package org.activiti.Repositories;


import org.activiti.Models.ECM_CASE;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ECMCaseRepository extends JpaRepository<ECM_CASE, Long> {

    List<ECM_CASE> findAllByIdIsNotNull();

    ECM_CASE findByCaseRK(String caseRK);
}