package org.activiti.Repositories;


import org.activiti.Models.ECMCaseProcessInstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ECMCaseProcessInstanceRepository extends JpaRepository<ECMCaseProcessInstance, Long> {

    ECMCaseProcessInstance findById(Long id);

    ECMCaseProcessInstance findFirstByCaseRK(String caseRK);

    ECMCaseProcessInstance findFirstByInstanceId(String instanceId);
}