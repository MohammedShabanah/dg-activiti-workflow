package org.activiti.Repositories;


import org.activiti.Models.WorkflowCaseHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkflowCasesRepository extends JpaRepository<WorkflowCaseHistory, Long> {

    List<WorkflowCaseHistory> findByCaseRK(String caseRK);


    List<WorkflowCaseHistory> findByInstanceId(String instanceId);

    WorkflowCaseHistory findById(Long id);


    WorkflowCaseHistory findFirstByCaseRK(String caseRK);


}