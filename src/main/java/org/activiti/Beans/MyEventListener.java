package org.activiti.Beans;


import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;

import static org.activiti.engine.delegate.event.ActivitiEventType.JOB_EXECUTION_SUCCESS;

public class MyEventListener implements ActivitiEventListener {

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {
            case PROCESS_STARTED:
                System.out.println("New Process Instance has been started.");
                break;

            case PROCESS_COMPLETED:

                String message = "Test Message";
//                        (String) event.getEngineServices().getRuntimeService().getVariable(event.getExecutionId(),
//                        "message");

                System.out.println("Process (started by \"" + message + "\") has been completed.");
                break;

            case VARIABLE_CREATED:
                System.out.println("New Variable was created.");
                System.out.println(">> All Variables in execution scope: ");
                break;

            case TASK_ASSIGNED:
                System.out.println("Task has been assigned.");
                break;

            case TASK_CREATED:
                System.out.println("Task has been created.");
                break;

            case TASK_COMPLETED:
                System.out.println("Test Message");
                break;

            default:
                break;
        }
    }

    @Override
    public boolean isFailOnException() {
        System.out.println("inside isFailOnException()");
        return false;
    }

}