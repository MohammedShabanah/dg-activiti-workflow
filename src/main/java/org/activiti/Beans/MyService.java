package org.activiti.Beans;


import org.activiti.Models.Person;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("myService")
public class MyService  {

    public void testCall(DelegateExecution execution){
        System.out.println("test call function");

        execution.setVariable("testVar","Mohammed Ali");
        Person person = new Person("Mohammed ",new Date());
        execution.setVariable("person",person);
    }


    public boolean testCondition(DelegateExecution execution){

        return false;
    }
}
