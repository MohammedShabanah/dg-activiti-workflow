package org.activiti.Beans;


import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Component;

@Component("helloWorldBean")
public class HelloWorldBean {

    public void sayHello(DelegateExecution execution) {
        System.out.println("Hello from " + this);
        execution.setVariable("var3", " from the bean");
    }

    public void testListener(DelegateExecution execution){
        System.out.println("Execution");
    }

}