package org.activiti.Beans;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

public class CustomTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        // get required variables
        String selectedPerson = (String) delegateTask.getVariable("selectedPerson");
        String selectedGroup = (String) delegateTask.getVariable("selectedGroup");

        if(null == selectedPerson || selectedPerson.isEmpty()){
            selectedPerson = "testPerson";
            delegateTask.setAssignee(selectedPerson);
        } else {
            delegateTask.setAssignee(selectedPerson);
        }
        if(null == selectedGroup || selectedGroup.isEmpty()){
            selectedGroup = "myGroup";
            delegateTask.addCandidateGroup(selectedGroup);
        } else {
            delegateTask.addCandidateGroup(selectedGroup);
        }

    }

}
