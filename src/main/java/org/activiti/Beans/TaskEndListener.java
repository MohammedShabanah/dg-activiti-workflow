package org.activiti.Beans;

import org.activiti.Models.Person;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;


public class TaskEndListener implements TaskListener {

    public void notify(DelegateTask delegateTask) {
        System.out.println("********* notify  ::: TaskEndListener");

        delegateTask.setVariable("myVar","TaskEndListener");
        Object object = delegateTask.getVariable("testVar");
        String testVar = (String) object;
        object = delegateTask.getVariable("person");

        Person person = (Person) object;

        System.out.println("end");

    }


}
