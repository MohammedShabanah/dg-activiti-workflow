package org.activiti.Beans;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class CustomDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println("custom delegate");

       String x =  delegateExecution.getCurrentActivityId();
       String y =  delegateExecution.getProcessInstanceId();
       String z =  delegateExecution.getProcessDefinitionId();

       delegateExecution.setVariable("myVar","test");

        Object object = delegateExecution.getVariable("myVar");

        // get required variables
        String selectedPerson = (String) delegateExecution.getVariable("selectedPerson");
        String selectedGroup = (String) delegateExecution.getVariable("selectedGroup");

        if(null == selectedPerson || selectedPerson.isEmpty()){
            selectedPerson = "testPerson";
            delegateExecution.setVariable("selectedPerson",selectedPerson);
        } else {
            delegateExecution.setVariable("selectedPerson",selectedPerson);
        }
        if(null == selectedGroup || selectedGroup.isEmpty()){
            selectedGroup = "myGroup";
            delegateExecution.setVariable("selectedGroup",selectedGroup);
        } else {
            delegateExecution.setVariable("selectedGroup",selectedGroup);
        }

        System.out.println("");

    }

    public void testCall(){
        System.out.println("test call function");
    }
}
